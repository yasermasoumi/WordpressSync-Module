﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using Security_Module;

namespace WordpressSync_Module
{
    public class clsLocalConfig
{
    private const string cstrID_Ontology = "984b33f9c9b54be08dfe66ebc8607c4b";
    private ImportWorker objImport;

    public Globals Globals { get; set; }

    private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
    public clsOntologyItem OItem_BaseConfig { get; set; }

    private OntologyModDBConnector objDBLevel_Config1;
    private OntologyModDBConnector objDBLevel_Config2;

        public clsOntologyItem UserItem { get; set; }
        public clsSecurityWork SecurityWork { get; set; }

        public clsOntologyItem OItem_class_url { get; set; }
    public clsOntologyItem OItem_class_user { get; set; }
    public clsOntologyItem OItem_class_web_service { get; set; }
    public clsOntologyItem OItem_object_baseconfig { get; set; }
    public clsOntologyItem OItem_relationtype_accessible_by { get; set; }
    public clsOntologyItem OItem_relationtype_belonging { get; set; }
    public clsOntologyItem OItem_relationtype_secured_by { get; set; }



    private void get_Data_DevelopmentConfig()
    {
        var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

        var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
        if (objOItem_Result.GUID == Globals.LState_Success.GUID)
        {
            if (objDBLevel_Config1.ObjectRels.Any())
            {

                objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                }).ToList();

                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingClass.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingObject.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                }));

                objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                {
                    if (!objDBLevel_Config2.ObjectRels.Any())
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }
            }
            else
            {
                throw new Exception("Config-Error");
            }

        }

    }

    public clsLocalConfig()
    {
        Globals = new Globals();
        set_DBConnection();
        get_Config();
    }

    public clsLocalConfig(Globals Globals)
    {
        this.Globals = Globals;
        set_DBConnection();
        get_Config();
    }

    private void set_DBConnection()
    {
        objDBLevel_Config1 = new OntologyModDBConnector(Globals);
        objDBLevel_Config2 = new OntologyModDBConnector(Globals);
        objImport = new ImportWorker(Globals);
    }

    private void get_Config()
    {
        try
        {
            get_Data_DevelopmentConfig();
            get_Config_AttributeTypes();
            get_Config_RelationTypes();
            get_Config_Classes();
            get_Config_Objects();
        }
        catch (Exception ex)
        {
            var objAssembly = Assembly.GetExecutingAssembly();
            AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            var strTitle = "Unbekannt";
            if (objCustomAttributes.Length == 1)
            {
                strTitle = objCustomAttributes.First().Title;
            }
           
            var objOItem_Result = objImport.ImportTemplates(objAssembly);
            if (objOItem_Result.GUID != Globals.LState_Error.GUID)
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            else
            {
                throw new Exception("Config not importable");
            }
           
        }
    }

    private void get_Config_AttributeTypes()
    {

    }

    private void get_Config_RelationTypes()
    {
        var objOList_relationtype_accessible_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_accessible_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_accessible_by.Any())
        {
            OItem_relationtype_accessible_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_accessible_by.First().ID_Other,
                Name = objOList_relationtype_accessible_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_accessible_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_belonging".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_belonging.Any())
        {
            OItem_relationtype_belonging = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging.First().ID_Other,
                Name = objOList_relationtype_belonging.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_secured_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_secured_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_secured_by.Any())
        {
            OItem_relationtype_secured_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_secured_by.First().ID_Other,
                Name = objOList_relationtype_secured_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_secured_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Objects()
    {
        var objOList_object_baseconfig = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "object_baseconfig".ToLower() && objRef.Ontology == Globals.Type_Object
                                          select objRef).ToList();

        if (objOList_object_baseconfig.Any())
        {
            OItem_object_baseconfig = new clsOntologyItem()
            {
                GUID = objOList_object_baseconfig.First().ID_Other,
                Name = objOList_object_baseconfig.First().Name_Other,
                GUID_Parent = objOList_object_baseconfig.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Classes()
    {
        var objOList_class_url = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "class_url".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_class_url.Any())
        {
            OItem_class_url = new clsOntologyItem()
            {
                GUID = objOList_class_url.First().ID_Other,
                Name = objOList_class_url.First().Name_Other,
                GUID_Parent = objOList_class_url.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_user = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "class_user".ToLower() && objRef.Ontology == Globals.Type_Class
                                   select objRef).ToList();

        if (objOList_class_user.Any())
        {
            OItem_class_user = new clsOntologyItem()
            {
                GUID = objOList_class_user.First().ID_Other,
                Name = objOList_class_user.First().Name_Other,
                GUID_Parent = objOList_class_user.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_web_service = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "class_web_service".ToLower() && objRef.Ontology == Globals.Type_Class
                                          select objRef).ToList();

        if (objOList_class_web_service.Any())
        {
            OItem_class_web_service = new clsOntologyItem()
            {
                GUID = objOList_class_web_service.First().ID_Other,
                Name = objOList_class_web_service.First().Name_Other,
                GUID_Parent = objOList_class_web_service.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }
}

}