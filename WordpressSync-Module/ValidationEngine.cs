﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WordPress_Module;
using WordpressSync_Module.Converter;

namespace WordpressSync_Module
{
    [Flags]
    public enum BlogQueryInfoValidationResult
    {
        None = 0,
        EmptyUrl = 1,
        WrongUriFormat = 2,
        EmptyUsername = 4,
        WrongUsernameFormat = 8,
        EmptyPassword = 16,
        Unknown = 32
    }

    [Flags]
    public enum PostListInfoValidationResult
    {
        None = 0,
        EmptyUrl = 1,
        WrongUriFormat = 2,
        EmptyUsername = 4,
        WrongUsernameFormat = 8,
        EmptyPassword = 16,
        NoBlogItem = 32,
        WrongBlogItemFormat = 64,
        Unknown = 128
    }
    public static class ValidationEngine
    {
        private static string regexPatternUsername = "^[a-z0-9_-]{3,15}$";

        public static BlogQueryInfoValidationResult IsOk_BlogQueryInfo(string userName, string password, string baseUrl)
        {
            var blogQueryInfoError = BlogQueryInfoValidationResult.None;

            if (string.IsNullOrEmpty(baseUrl)) blogQueryInfoError |= BlogQueryInfoValidationResult.EmptyUrl;

            Uri baseUri;
            if (!Uri.TryCreate(baseUrl, UriKind.Absolute, out baseUri)) blogQueryInfoError |= BlogQueryInfoValidationResult.WrongUriFormat;

            if (!string.IsNullOrEmpty(userName))
            {
                var regexTesterUsername = new Regex(regexPatternUsername);

                if (!regexTesterUsername.Match(userName).Success) blogQueryInfoError |= BlogQueryInfoValidationResult.WrongUsernameFormat;
            }
            else
            {
                blogQueryInfoError |= BlogQueryInfoValidationResult.EmptyUsername;
            }

            if (string.IsNullOrEmpty(password)) blogQueryInfoError |= BlogQueryInfoValidationResult.EmptyPassword;

            return blogQueryInfoError;
        }

        public static PostListInfoValidationResult IsOk_PostListInfo(string userName, string password, string baseUrl, UserBlog userBlog)
        {
            var blogQueryInfoValidation = IsOk_BlogQueryInfo(userName, password, baseUrl);

            var loginInfoValidation = (PostListInfoValidationResult) (new QueryInfoResultToLoginInfoResultConverter()).Convert(blogQueryInfoValidation, typeof(PostListInfoValidationResult), null, System.Globalization.CultureInfo.CurrentCulture);

            if (userBlog != null)
            {
                if (!userBlog.IsValid())
                {
                    loginInfoValidation |= PostListInfoValidationResult.WrongBlogItemFormat;
                }
            }
            else
            {
                loginInfoValidation |= PostListInfoValidationResult.NoBlogItem;
            }

            return loginInfoValidation;

        }
    }
}
