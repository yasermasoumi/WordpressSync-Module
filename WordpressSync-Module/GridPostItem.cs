﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordPress_Module;
using WordpressSync_Module.Attributes;
using WordpressSync_Module.BaseClasses;

namespace WordpressSync_Module
{
    public class GridPostItem : NotifyPropertyChange
    {
        [Visible(false)]
        private PostItem postItem;

        
        [Visible(true)]
        [Readonly(true)]
        public string Guid
        {
            get { return postItem.Guid; }
            set
            {
                postItem.Guid = value;
            }
        }

        [Visible(true)]
        [Readonly(true)]
        public int Id
        {
            get { return int.Parse(postItem.Id); }
            set
            {
                postItem.Id = value.ToString();
            }
        }

        [Visible(true)]
        public string Title
        {
            get { return postItem.Title; }
            set
            {
                postItem.Title = value;
            }
        }

        [Visible(true)]
        [Readonly(true)]
        public DateTime Created
        {
            get { return postItem.Created; }
            set
            {
                postItem.Created = value;
            }
        }

        [Visible(true)]
        [Readonly(true)]
        public string Status
        {
            get { return postItem.Status; }
            set
            {
                postItem.Status = value;
            }
        }

        [Visible(true)]
        [Readonly(true)]
        public string Type
        {
            get { return postItem.Type; }
            set
            {
                postItem.Type = value;
            }
        }

        [Visible(true)]
        [Readonly(true)]
        public string Name
        {
            get { return postItem.Name; }
            set
            {
                postItem.Name = value;
            }
        }


        [Visible(true)]
        [Readonly(true)]
        public string Author
        {
            get { return postItem.Author; }
            set
            {
                postItem.Author = value;
            }
        }

        [Visible(true)]
        [Readonly(true)]
        public string Parent
        {
            get { return postItem.Parent; }
            set
            {
                postItem.Parent = value;
            }
        }

        [Visible(true)]
        [Readonly(true)]
        public string Link
        {
            get { return postItem.Link; }
            set
            {
                postItem.Link = value;
            }
        }

        private string termItems;
        [Visible(true)]
        [Readonly(true)]
        public string TermItems
        {
            get { return termItems; }
            set
            {
                termItems = value;
            }
        }

        public GridPostItem(PostItem postItem)
        {
            this.postItem = postItem;
            this.postItem.PropertyChanged += PostItem_PropertyChanged;
            TermItems = string.Join(", ", postItem.TermItems.Select(termItem => termItem.Name));
        }

        private void PostItem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(e.PropertyName);
            TermItems = string.Join(", ", postItem.TermItems.Select(termItem => termItem.Name));
        }
    }
}
